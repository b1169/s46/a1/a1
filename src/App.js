import {Fragment} from "react";
import {Container} from "react-bootstrap";
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import Courses from './pages/Courses'

function App() {

  return(
    <Fragment>
      <AppNavbar />

      <Container>
      <Home />
      <Courses />
      </Container>
      
    </Fragment>
  )
}

export default App;
