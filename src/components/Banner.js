function Banner(){
	return(

		<div className ="container-fluid mt-2">
			<div className ="row justify-content-center">
			<div className="col-10 col-md-8">

			<div className="jumbotron">

			<h1>Welcome to Bootcamp</h1>
			<p>Learn Coding</p>
			<button className="btn btn-primary">Click Here</button>
			</div>

			</div>
			</div>
		</div>
	)
}

export default Banner;