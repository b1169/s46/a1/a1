import { useState } from 'react';
import {Card,Row, Col, Container, Button} from "react-bootstrap";
import PropTypes from 'prop-types';

function CourseCard({courseProp}){

	console.log(courseProp)

	const {name, description, price} = courseProp

	//Use the state hook for this component to be able to store its state
	//states are used to keep track of information related to individual components
	/*
		syntax:
			const [getter, setter] = useState(initialGetterValue)

			getter is default value
			setter sets updated value
	*/

	const [count, setCount] = useState(0)
	console.log(useState(0))
	const [count1, seatCount1] = useState(10)


	function enroll(){
		setCount(count + 1);
		console.log('Enrollees: ' + count)

		if(count1 > 0){
				seatCount1(count1 - 1)

		}

		else{
			alert('No more seats available')
		}
	}

	/*1. Create a seats state in the CourseCard component and set the initial value to 10.*/

	

	/*2. For every enrollment, deduct one to the seats.*/

	/*3. If the seats reaches zero do the following:
	- Do not add to the count.
	- Do not deduct to the seats.
	-Show an alert that says No more seats available.*/


	return(

		<Container fluid className = "mb-4">
			<Row>
				<Col xs = {12} md = {4}>

				<Card className="cardHighlights p-3">
			
				  <Card.Body>
				    <Card.Title>${name}</Card.Title>
				    <Card.Text>
				      Description:</Card.Text>
				      <Card.Subtitle>${description}</Card.Subtitle>
				       <Card.Text>Price: </Card.Text> <Card.Subtitle>${price}</Card.Subtitle>
				      <Card.Text>Enrolless: {count}</Card.Text>
				      <Button variant="primary" onClick={enroll}>Enroll Now</Button>
				  </Card.Body>

				</Card>

				</Col>
			</Row>
		</Container>
		)

}



// Check if the CourseCard component is getting the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from component to the next


//propTypes vs PropType dun sa taas
CourseCard.propTypes ={
	//shape method used to check if a prop object conforms to a specific shape
	courseProp: PropTypes.shape({
		//define the properties and their expected types

		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

export default CourseCard;