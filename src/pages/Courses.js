import coursesData from './../data/coursesData'

/*components*/
import CourseCard from './../components/CourseCard'

function Courses(){

	//check kung may nakita

	console.log(coursesData)

	const courses = coursesData.map(course => {

		return(
			<CourseCard key={course.id} courseProp={course}/>
			)
	})

	return(
		<>
			{courses}
		</>
	)
}

export default Courses;